<?php

namespace App\Commands;

use App\Helpers\NumberFixePrefix;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class StartCommand extends Command
{
    public NumberFixePrefix $n;
    public function __construct()
    {
        parent::__construct();
        $this->n = new NumberFixePrefix();
    }

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'start';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Lancement du programme';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $option = $this->menu('Bienvenue !, Veuillez selectionner un type de numéro', [
            'Fixe',
            'Mobile'
        ])->open();

        $this->info($option);

        if($option == 0) {
            $fixe_type = $this->menu("Choix du type de numéro fixe", [
                "Geographique",
                "IP"
            ])->open();

            if($fixe_type == 0) {

                $geo_code = $this->menu("Choix du secteur géographique", [
                    'Ile de France',
                    'Nord Ouest',
                    'Nord Est',
                    'Sud Est',
                    'Sud Ouest'
                ])->open();

                $this->call("generate:geographique", ["sector" => $geo_code]);
            } else {
                $this->line($this->n->generateNumberFixe());
            }
        } else {
            $this->line($this->n->generateNumberMobile());
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
