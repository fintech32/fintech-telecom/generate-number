<?php

namespace App\Helpers;

use Faker\Factory;
use Illuminate\Support\Arr;

class NumberFixePrefix
{
    public function getListPrefix()
    {
        return collect(
            [
                [
                    'sector' => "Ile de France",
                    'number' => '01',
                ],
                [
                    'sector' => "Nord Ouest",
                    'number' => '02',
                ],
                [
                    'sector' => "Nord Est",
                    'number' => '03',
                ],
                [
                    'sector' => "Sud Est",
                    'number' => '04',
                ],
                [
                    'sector' => "Sud Ouest",
                    'number' => '05',
                ],
            ],
        );
    }

    public function generateNumberFixe($sector = null) {
        $faker = Factory::create('fr_FR');
        $all = $this->getListPrefix()->toArray();
        $data = Arr::get($all, $sector);

        if($sector) {
            return "NUMERO: ".$data['number'].$faker->randomNumber(8);
        } else {
            return "NUMERO: 09".$faker->randomNumber(8);
        }
    }

    public function generateNumberMobile()
    {
        $faker = Factory::create('fr_FR');
        return "NUMERO: 0".rand(6,7).$faker->randomNumber(8);
    }


}
